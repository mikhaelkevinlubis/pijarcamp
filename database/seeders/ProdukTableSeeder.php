<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Produk;

class ProdukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::insert([
        [
            'nama_produk' => "Sabun",
            'keterangan' => NULL, 
            'harga' => 5000,
            'jumlah' => 200
        ],
        [
            'nama_produk' => "Pasta Gigi",
            'keterangan' => NULL, 
            'harga' => 3500,
            'jumlah' => 150
        ]
    ]);
    }
}
