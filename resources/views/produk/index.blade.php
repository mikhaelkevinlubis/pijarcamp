@extends('layouts.master')
@section('judul', "Daftar Produk")
@section('konten')
<div class="row d-flex justify-content-center">
    <div class="card">
        <div class="card-title">
            <h1 style="text-align: center;" class="mt-2">
                Daftar Produk
            </h1>
        </div>
        <div class="card-body">
            <table class="table table-hover table-responsive w-100">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Produk</th>
                        <th>Keterangan</th>
                        <th>Harga</th>
                        <th>Jumlah</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($produk as $key => $item)
                    <tr>
                        <td>{{ $key+1; }}</td>
                        <td>{{ $item->nama_produk; }}</td>
                        <td>{{ $item->keterangan; }}</td>
                        <td>{{ $item->harga; }}</td>
                        <td>{{ $item->jumlah; }}</td>
                        <td>
                            <form action="produk/{{ $item->id }}" method="post">
                                <a href="produk/{{ $item->id }}" class="btn btn-outline-primary">Detail</a>
                                <a href="produk/{{ $item->id }}/edit" class="btn btn-outline-warning">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                            </form>
                        </td>
                    </tr>  
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
    
@endsection