@extends('layouts.master')
@section('judul', "Daftar Produk")
@section('konten')
<div class="row d-flex justify-content-center">
    <div class="card w-50 ">
        <div class="card-title">
            <h1 style="text-align: center;" class="mt-2">
                Detail Produk
            </h1>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-3">
                    <label for="namaProduk"><b>Nama Produk: </b></label>
                    <p id="namaProduk"> {{ $produk->nama_produk }}</p>
                </div>
                <div class="col-4">
                    <label for="keterangan"><b>Keterangan: </b></label>
                    <p id="keterangan"> {{ $produk->keterangan }}</p>
                </div>
                <div class="col-2">
                    <label for="harga"><b>Harga: </b></label>
                    <p id="harga"> {{ $produk->harga }}</p>
                </div>
                <div class="col-2">
                    <label for="jumlah"><b>Jumlah: </b></label>
                    <p id="jumlah"> {{ $produk->jumlah }}</p>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <a href="/produk" class="btn btn-outline-primary">Back</a>
            </div>
        </div>
    </div>
</div>
    
@endsection