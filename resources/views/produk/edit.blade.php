@extends('layouts.master')
@section('judul', "Edit Produk")
@section('konten')
<div class="row d-flex justify-content-center">
    <div class="card w-50 ">
        <div class="card-title">
            <h1 style="text-align: center;" class="mt-2">
                Edit Produk
            </h1>
        </div>
        <div class="card-body">
            <form action="/produk/{{ $produk->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="namaProduk"><b>Nama Produk</b></label>
                    <input type="text" name="nama_produk" id="namaProduk" class="form-control" value="{{ $produk->nama_produk }}">
                </div>
                <div class="mb-3">
                    <label for="keteranganProduk"><b>Keterangan</b></label>
                    <textarea name="keterangan" class="form-control" id="keteranganProduk" cols="20" rows="5" value="{{ $produk->keterangan }}"></textarea>
                </div>
                <div class="row mb-3">
                    <div class="col-6">
                        <label for="hargaProduk"><b>Harga</b></label>
                        <input type="number" name="harga" id="hargaProduk" class="form-control w-100" value="{{ $produk->harga }}">
                    </div>
                    <div class="col-6 ">
                        <label for="jumlahProduk"><b>Jumlah</b></label>
                        <input type="number" name="jumlah" id="jumlahProduk" class="form-control w-50" value="{{ $produk->jumlah }}">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
    
@endsection