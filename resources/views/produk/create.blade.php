@extends('layouts.master')
@section('judul', "Tambah Produk")
@section('konten')
<div class="row d-flex justify-content-center">
    <div class="card w-50 ">
        <div class="card-title">
            <h1 style="text-align: center;" class="mt-2">
                Tambah Produk
            </h1>
        </div>
        <div class="card-body">
            <form action="/produk" method="post">
                @csrf
                <div class="mb-3">
                    <label for="namaProduk"><b>Nama Produk</b></label>
                    <input type="text" name="nama_produk" id="namaProduk" class="form-control" placeholder="Masukkan nama produk...">
                </div>
                <div class="mb-3">
                    <label for="keteranganProduk"><b>Keterangan</b></label>
                    <textarea name="keterangan" class="form-control" id="keteranganProduk" cols="20" rows="5" placeholder="Masukkan keterangan produk disini..."></textarea>
                </div>
                <div class="row mb-3">
                    <div class="col-6">
                        <label for="hargaProduk"><b>Harga</b></label>
                        <input type="number" name="harga" id="hargaProduk" class="form-control w-100">
                    </div>
                    <div class="col-6 ">
                        <label for="jumlahProduk"><b>Jumlah</b></label>
                        <input type="number" name="jumlah" id="jumlahProduk" class="form-control w-50">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
    
@endsection